﻿using System;
using System.IO;
using System.Media;
using System.Windows;
using System.Xml;

namespace ReminderWPF
{
    class Audio 
    //работа с проигрыванием
    {
        SoundPlayer sp;
        int h;
        int m;
        bool b;
        string Savepath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @"\save.xml";
        string[] LocationSounds;
        string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location).ToString();
        public string Alarm { get; set; }
        public bool Show {get;set; }

        public Audio()
        {
            Show = false;
            LocationSounds = new string[]{
path+ @"\Resources\Audio\sound00.wav", path+ @"\Resources\Audio\sound01.wav", path+ @"\Resources\Audio\sound02.wav"};
        }

        public void Play(Sound s)
        {
            string name = s.ReminderName;
            int hReminder = s.ReminderHour;
            int mReminder = s.ReminderMinute;          
            int location = s.ReminderSound;
            bool check = s.ReminderChecked;

            Console.WriteLine(name + " " + hReminder + " " + mReminder);
            //проигрывает звук по времени
            h = DateTime.Now.Hour;
            m = DateTime.Now.Minute;

            if (check)
         
            {
                if (hReminder == h & mReminder == m)
                {
                    Show = true;
                    sp = new SoundPlayer(); //запуск аудио
                    
                    sp.SoundLocation = LocationSounds[location];
                    Console.WriteLine(LocationSounds[location]);
                    sp.Load();                 
                        sp.Play();
                        if (b)  //надпись
                            Alarm = "";
                        else
                            Alarm = name;
                        b = !b;                 
                }
                else
                { 
                    Alarm = "ghf";
                }
            } 
        }

        //сохраняет настройки будильника
      public  void Save(string n, string h, string m)
        {         
            XmlDocument document = new XmlDocument();

            if (File.Exists(Savepath))
            {
                document.Load(Savepath);
                foreach (XmlNode autoruns in document.GetElementsByTagName("reminder1"))
                {
                    if (autoruns.Attributes["name"].Value != null)
                    {
                        autoruns.Attributes["name"].Value = n;
                    }
                    if (autoruns.Attributes["hour"].Value != null)
                    {
                        autoruns.Attributes["hour"].Value = h;
                    }
                    if (autoruns.Attributes["minute"].Value != null)
                    {
                        autoruns.Attributes["minute"].Value = m;
                    }
                }
                document.Save(Savepath);
            }
        }
    }
}
