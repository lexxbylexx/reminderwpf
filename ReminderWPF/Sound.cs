﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReminderWPF
{
    public class Sound
    {

        public string ReminderName { get; set; }
        public int ReminderHour { get; set; }
        public int ReminderMinute { get; set; }
        public int ReminderSound { get; set; }
        public bool ReminderChecked { get; set; }
       

        public Sound()
        {
           
        }

        public override string ToString()
        {
            return ReminderName + " " + ReminderHour + " " + ReminderMinute + " " + ReminderSound + " " + ReminderChecked;
        }
    }
}
