﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using System.Xml;
using Winforms = System.Windows.Forms;
namespace ReminderWPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
   
    public partial class MainWindow : Window
    {
       

        public bool BExit { get; set; }

        private bool bSec;
        private bool bMin;
        private bool bHour;

        bool bClocks = false;
        DispatcherTimer timerSec = new DispatcherTimer();
        Winforms.NotifyIcon ni = new System.Windows.Forms.NotifyIcon();
        System.Windows.Controls.ContextMenu menu;
        public event PropertyChangedEventHandler PropertyChanged;
        public int h, m, images;
        string s;
        public bool bTimer, boo;
        public string Savepath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @"\save.xml";
        XmlDocument document = new XmlDocument();
        Autorun autorun = new Autorun();
        Audio audio = new Audio();
        List<Sound> sounds = new List<Sound>();
        List<String> arrayimages = new List<String>();
        SettingsRem  set = new SettingsRem();
        bool bAnim;
        public MainWindow()
        {
            InitializeComponent();
            time();
            Load();
            ni.Icon = new System.Drawing.Icon("clock.ico");
            menu = getMenu();
            BExit = false;

            //запуск таймера и часики становятся поверх всех окон
            timerSec.Interval = new TimeSpan(0, 0, 0, 1, 0);
            timerSec.Tick += new EventHandler(timer_TickSec);
            timerSec.Start();
            this.Topmost = true;
        }
        public void NotifyPropertyChanged(string PropertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(PropertyName));
            }
        }

        // запускает каждую секунду, обновляя изображение
        private void timer_TickSec(object sender, EventArgs e)
        {
            //анимация тряски будильника
            //if (bAnim)
            //{
            //    (clocks_analog.RenderTransform as RotateTransform).Angle += 12;
            //}
            //else
            //{
            //    (clocks_analog.RenderTransform as RotateTransform).Angle -= 24;
            //}
            //bAnim = !bAnim;

            if ((second.RenderTransform as RotateTransform).Angle == 360 && !bSec)
            {
                (second.RenderTransform as RotateTransform).Angle = 0;
                bSec = true;
            }
            else
                bSec = false;

            (second.RenderTransform as RotateTransform).Angle = DateTime.Now.Second * 6;

            if ((int)(minute.RenderTransform as RotateTransform).Angle == 360 && !bMin)
            {
                (minute.RenderTransform as RotateTransform).Angle = 0;
                bMin = true;
            }
            else
                bMin = false;
            //более гладкий поворот стрелки, учитываем малый градус в секундах
            (minute.RenderTransform as RotateTransform).Angle = (double)DateTime.Now.Minute * 6 + (double)DateTime.Now.Second * 0.1;

            if ((int)(hour.RenderTransform as RotateTransform).Angle == 360 && !bHour)
            {
                (hour.RenderTransform as RotateTransform).Angle = 0;
                bHour = true;
            }
            else
                bHour = false;

            (hour.RenderTransform as RotateTransform).Angle = (double)(DateTime.Now.Hour % 12) * 30 + (double)DateTime.Now.Minute * 0.5;

        }
        //отображение времени
        public void time()             
       { 
            DispatcherTimer uiTimer = new DispatcherTimer();
            uiTimer.Tick += (sender, e) =>
            {
                h = DateTime.Now.Hour;
                m = DateTime.Now.Minute;
                if(bTimer==true)
                    s = "   ";
                else
                    s = " : ";
                bTimer = !bTimer;
                if (m > 9)
                {
                    timetext.Text = h + s + m;
                }
                else 
                {
                    timetext.Text = h + s + "0" + m; 
                }

                if (set._ListSounds.Count != 0)
                {
                    audio.Play(set._ListSounds[0]);
                    audio.Play(set._ListSounds[1]);
                    audio.Play(set._ListSounds[2]);
                    audio.Play(set._ListSounds[3]);
                    audio.Play(set._ListSounds[4]);

                }
                if (audio.Show)
                {
                    this.Show();
                    this.Activate();
                    audio.Show = false;
                }

                NotifyPropertyChanged("timetext");

                alarm.Content = audio.Alarm;
            };
            uiTimer.Interval = TimeSpan.FromSeconds(2);            
           uiTimer.Start();
           
        }

        //перетаскивание формы за любой элемент
        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
          
        }

        //метод для загрузки данных автозапуска, будильников и в листфью
        private void Load()       
        {
            XmlNode autorunX, reminder1;
            XmlAttribute state,name,h,m, img;
            if (!File.Exists(Savepath))
                //создаем сэйв, если его нет
            {
                XmlTextWriter textWritter = new XmlTextWriter(Savepath, Encoding.UTF8);
                textWritter.WriteStartDocument();
                textWritter.WriteStartElement("head");
                textWritter.WriteEndElement();
                textWritter.Close();

                document.Load(Savepath);

                autorunX = document.CreateElement("autorun");
                state = document.CreateAttribute("state");
                img = document.CreateAttribute("images");
                document.DocumentElement.AppendChild(autorunX);
                autorunX.Attributes.Append(state);
                autorunX.Attributes.Append(img);
                state.Value = "False";
                img.Value = "0";

                reminder1 = document.CreateElement("reminder1");
                name = document.CreateAttribute("name");
                h = document.CreateAttribute("hour");
                m = document.CreateAttribute("minute");
                reminder1.Attributes.Append(name);
                reminder1.Attributes.Append(h);
                reminder1.Attributes.Append(m);
                document.DocumentElement.AppendChild(reminder1);
                
                document.Save(Savepath);
            }
            else
                //загружаем готовый сэйв
            {
                document.Load(Savepath);
                foreach (XmlNode autoruns in document.GetElementsByTagName("autorun"))
                {
                    if (autoruns.Attributes["images"] != null)
                    {
                        images = Int32.Parse(autoruns.Attributes["images"].Value);
                    }
                }
                foreach (XmlNode reminders in document.GetElementsByTagName("reminder1"))
                {
                    if (reminders.Attributes["name"].Value != null
                        && reminders.Attributes["hour"].Value != null
                            && reminders.Attributes["minute"].Value != null)
                    {
                        //reminderName1.Text = reminders.Attributes["name"].Value;
                        //reminderHour1.Text = reminders.Attributes["hour"].Value;
                        //reminderMinute1.Text = reminders.Attributes["minute"].Value;
                    }
                }
                document.Save(Savepath);
            }
            //установить фон часиков
            background.Source = new BitmapImage(
         new Uri(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location).ToString()
         + @"\Resources\Image\background"
         + (images).ToString()
         + ".png"));

            //установить иконку
            this.Icon = new BitmapImage(new Uri("clock.ico", UriKind.Relative));           
            //установить прозрачность фона
            this.Background = new SolidColorBrush(Color.FromArgb(0, 34, 34, 34));
            //упрятать приложение в трэй
            ni.Visible = false;
            ni.DoubleClick += (sndr, args) =>
            {
                this.Show();
                this.WindowState = WindowState.Normal;
            };
            ni.Click += (sndr, args) =>
            {
                if ((args as Winforms.MouseEventArgs).Button == Winforms.MouseButtons.Right)
                    menu.IsOpen = true;
            };

            //загружаем список будильников из сэйва
            //StreamReader sr = new StreamReader("save.txt");

            set._ListSounds = new List<Sound>();
            //string s;
            //char[] splitChars = new char[] { '\n', '\r' };
            //while ((s = sr.ReadLine()) != (null))
            //{
            //    Sound deserializedSound;
            //    //убрать пустые строки
            //    foreach (var item in s.Split(splitChars, StringSplitOptions.RemoveEmptyEntries))
            //        if (!string.IsNullOrEmpty(item.Trim()))
            //        {
            //            deserializedSound = JsonConvert.DeserializeObject<Sound>(s);
            //            set._ListSounds.Add(deserializedSound);

            //        }
            //}
                  
            //sr.Close();
        }

        //фильтруем часы
        private void reminderHour_TextChanged(object sender, TextChangedEventArgs e)           
        {
          //  reminderHour1.Text = RegularTime.RegularHour(reminderHour1.Text);       
        }
       
        //фильтруем минуты
        private void reminderMinute_TextChanged(object sender, TextChangedEventArgs e)          
        {
         //   reminderMinute1.Text = RegularTime.RegularMinute(reminderMinute1.Text);
        }

        //сменить картинку
        private void Image_Click(object sender, RoutedEventArgs e)
        {
            
            if (images < 20)
            {
                ++images;
            }
            else
            {
                images = 0;
            }
            document.Load(Savepath);

            foreach (XmlNode autoruns in document.GetElementsByTagName("autorun"))
            {
                if (autoruns.Attributes["images"] != null)
                {
                    autoruns.Attributes["images"].Value = images.ToString();
                }
            }
            document.Save(Savepath);
            background.Source = new BitmapImage(
new Uri(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location).ToString()
+ @"\Resources\Image\background"
+ images.ToString()
+ ".png"));

            
        }

        // открыть настройки
        private void SettingsClick(object sender, RoutedEventArgs e)
        {                  
                set.Show();                 
        }

        //меню для трея
        private ContextMenu getMenu()
        {
            var menu = new ContextMenu();
         //   menu.Items.Add(new MenuItem { Header = "first" });
            var mnuexit = new MenuItem { Header = "Exit" };
            mnuexit.Click += (sndr, args) => Application.Current.Shutdown();
            menu.Items.Add(mnuexit);
            return menu;          
        }
        
        //отобразить часики c аниамцией
        private void Clock_Click(object sender, RoutedEventArgs e)
        {

            var button = sender as Button;
            if (button == null)
                return;

            var animation1 = new ThicknessAnimation();
            var animation2 = new ThicknessAnimation();
            if (bClocks)
            {
                animation1 = new ThicknessAnimation
                {
                    From = new Thickness(0, 0, 0, 0),
                    To = new Thickness(0, 170, 0, 0),
                    Duration = TimeSpan.FromSeconds(1)
                };
                animation2 = new ThicknessAnimation
                {
                    From = new Thickness(0, 200, 0, 0),
                    To = new Thickness(22, 0, 22, 0),
                    Duration = TimeSpan.FromSeconds(0.6)
                };
                button_clocks.Source = new BitmapImage(new Uri(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location).ToString()
+ @"\Resources\Image\image_button.png"));
            }
            else
            {
                animation1 = new ThicknessAnimation
                {
                    From = new Thickness(0, 170, 0, 0),
                    To = new Thickness(0, 0, 0, 0),
                    Duration = TimeSpan.FromSeconds(1)
                };
                animation2 = new ThicknessAnimation
                {
                    From = new Thickness(22, 0, 22, 0),
                    To = new Thickness(0, 200, 0, 0),
                    Duration = TimeSpan.FromSeconds(0.6)
                };
                button_clocks.Source = new BitmapImage(new Uri(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location).ToString()
+ @"\Resources\Image\clock_button.png"));          
            }

            clocks_digital.BeginAnimation(MarginProperty, animation1);
            clocks_analog.BeginAnimation(MarginProperty, animation2);
            bClocks = !bClocks;
        }

        //закрыть приложение
        private void ClosedWindow_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
            this.Close();
        }
    } 
}
