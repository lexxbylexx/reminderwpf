﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ReminderWPF
{
    public static class RegularTime
    {
        //фильтр для часов
        public static string RegularHour(string hour)
        {
            string pattern1 = (@"([\D])|(^[^0-2][^0-9]$)");
            hour = Regex.Replace(hour, pattern1, "");
            if (hour.Length > 0)
                if (Int32.Parse(hour) > 23 | hour.Length > 2)
                    hour = "0";
            return hour;
        }

        //фильтр для минут
        public static string RegularMinute(string minute)
        {
            string pattern1 = (@"([\D])|(^[^0-2][^0-9]$)");
            minute = Regex.Replace(minute, pattern1, "");
            if (minute.Length > 0)
                if (Int32.Parse(minute) > 59 | minute.Length > 2)
                    minute = "0";
            return minute;
        }
    }
}
