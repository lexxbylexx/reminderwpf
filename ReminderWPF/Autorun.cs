﻿using Microsoft.Win32;
using System;
using System.IO;
using System.Text;
using System.Xml;

namespace ReminderWPF
{

    class Autorun
    {
        public Autorun()
        {
            b = B;
        }

        public bool B { get; set; }

        const string name = "Reminder";
        string Exepath = System.Reflection.Assembly.GetExecutingAssembly().Location;
        string Savepath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @"\save.xml";
        XmlDocument document = new XmlDocument();
        XmlDocument dataDocument = new XmlDocument();
        bool b;

        public bool SetAutorunValue(bool autorun)
        {
            Console.Write("авторан");
            RegistryKey reg;
            try
            {
                reg = Registry.CurrentUser.CreateSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\Run\\");

                if (autorun)
                    reg.SetValue(name, Exepath);
                else
                    reg.DeleteValue(name);
                reg.Close();
            }
            catch
            {
                return !autorun;
            }
            return !autorun;
        }


        public void Save(string s)
        {
            XmlDocument document = new XmlDocument();

            if (File.Exists(Savepath))
            {
                document.Load(Savepath);
                foreach (XmlNode autoruns in document.GetElementsByTagName("autorun"))
                {
                    if (autoruns.Attributes["state"].Value != null)
                    {
                        autoruns.Attributes["state"].Value = s;
                    }
                }
                document.Save(Savepath);
            }
        }
    }
}
