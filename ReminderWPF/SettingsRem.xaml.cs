﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Xml;

namespace ReminderWPF
{

    public partial class SettingsRem : Window
    {
        public List<Sound> _ListSounds { get; set; }

#region
        public string Savepath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @"\save.xml";
        XmlDocument document = new XmlDocument();
        string n = "---";
        int h = 0, m = 0, s = 0;
        bool c = false;
        bool boo;
        StringBuilder sb = new StringBuilder();
        int index;
        Autorun autorun = new Autorun();
        Sound sound = new Sound();
        
#endregion

        public SettingsRem()
        {
            this.Icon = new BitmapImage(new Uri("clock.ico", UriKind.Relative));
            InitializeComponent();
            Add();
            
        }

        //добавляем положение выключателя
        private void Add()
        {
            document.Load(Savepath);
            foreach (XmlNode autoruns in document.GetElementsByTagName("autorun"))
            {
                if (autoruns.Attributes["state"].Value != null )
                {
                    CheckBox_autorun.Checked -= CheckBox_autorun_Checked;
                    CheckBox_autorun.IsChecked = bool.Parse(autoruns.Attributes["state"].Value);
                    boo = bool.Parse(autoruns.Attributes["state"].Value);
                    autorun.SetAutorunValue(boo);
                    CheckBox_autorun.Checked += CheckBox_autorun_Checked;
                }
            }
        }

        //регулярные для минут и секунд
        private void Textchanged(object sender, TextChangedEventArgs e)
        {
            TextBox textbox = sender as TextBox;

            if (textbox.Name == "reminderhour1" || textbox.Name == "reminderhour2" || textbox.Name == "reminderhour3" || textbox.Name == "reminderhour4" || textbox.Name == "reminderhour5")
            {
                textbox.Text = RegularTime.RegularHour(textbox.Text); 
            }
            else
                textbox.Text = RegularTime.RegularMinute(textbox.Text);
       

        }

        //cохраняем настройки
        public void IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            //var element = sender as FrameworkElement;
            //switch (element.GetType().ToString())
            //{
            //    case "System.Windows.Controls.CheckBox":
            //       CheckBox checkbox = (CheckBox)element;
            //       c = (bool)checkbox.IsChecked;
            //       _ListSounds.Add(new Sound {ReminderName = n, ReminderHour = h, ReminderMinute = m, ReminderSound = s, ReminderChecked = c });
            //        break;
            //    case "System.Windows.Controls.TextBox":
            //        TextBox textbox = (TextBox)element;
            //    switch (textbox.Name)
            //        {
            //        case "textboxname":
            //                n = textbox.Text;
            //            break;
            //        case "textboxhour":
            //                h = Int32.Parse( textbox.Text);
            //            break;
            //        case "textboxminute":
            //                m = Int32.Parse(textbox.Text);
            //            break;
            //        }
            //        break;
            //    case "System.Windows.Controls.ComboBox":
            //        ComboBox cm = (ComboBox)element;
            //        s = cm.SelectedIndex;
            //        break;
            //    default:
            //        break;
            //}         
        }

        //загружаем список значений в меню - звуки
        public void Loaded(object sender, RoutedEventArgs e)
        {
            var element = sender as FrameworkElement;
            ComboBox cm = (ComboBox)element;
            cm.ItemsSource = (new string[] { "Монетка", "Писк", "Лазер", "Голос 1", "Голос 2", "Голос 3", "Голос 4", "Голос 5", "Голос 6", "Голос 7", "Голос 8" });        
        }

        //закрываем окно и сохраняем настройки в файл
        private void Exit(object sender, RoutedEventArgs e)
        {
            _ListSounds.Clear();
            WindowSettings.Hide();
            StreamWriter sw = new StreamWriter("save.txt");
            foreach (var item in _ListSounds)
            {
                sb.AppendLine(JsonConvert.SerializeObject(item));             ;
            }
            string s = sb.ToString();                 
            sw.WriteLine(s);
            sw.Close();

            _ListSounds.Add(new Sound {ReminderName = remindername1.Text, 
                ReminderHour = Int32.Parse(reminderhour1.Text), 
                ReminderMinute = Int32.Parse(reminderminute1.Text),
                ReminderChecked = ((bool)remindercheck1.IsChecked),
                ReminderSound = (reminderselect1.SelectedIndex)});

            _ListSounds.Add(new Sound
            {
                ReminderName = remindername2.Text,
                ReminderHour = Int32.Parse(reminderhour2.Text),
                ReminderMinute = Int32.Parse(reminderminute2.Text),
                ReminderChecked = ((bool)remindercheck2.IsChecked),
                ReminderSound = (reminderselect2.SelectedIndex)
            });

            _ListSounds.Add(new Sound
            {
                ReminderName = remindername3.Text,
                ReminderHour = Int32.Parse(reminderhour3.Text),
                ReminderMinute = Int32.Parse(reminderminute3.Text),
                ReminderChecked = ((bool)remindercheck3.IsChecked),
                ReminderSound = (reminderselect3.SelectedIndex)
            });

            _ListSounds.Add(new Sound
            {
                ReminderName = remindername4.Text,
                ReminderHour = Int32.Parse(reminderhour4.Text),
                ReminderMinute = Int32.Parse(reminderminute4.Text),
                ReminderChecked = ((bool)remindercheck4.IsChecked),
                ReminderSound = (reminderselect4.SelectedIndex)
            });

            _ListSounds.Add(new Sound
            {
                ReminderName = remindername5.Text,
                ReminderHour = Int32.Parse(reminderhour5.Text),
                ReminderMinute = Int32.Parse(reminderminute5.Text),
                ReminderChecked = ((bool)remindercheck5.IsChecked),
                ReminderSound = (reminderselect5.SelectedIndex)
            });
        }

        //переключатель для автозапуска
        private void CheckBox_autorun_Checked(object sender, RoutedEventArgs e)
        {
            CheckBox_autorun.Checked -= CheckBox_autorun_Checked;
            boo = autorun.B = !boo;
            autorun.SetAutorunValue(boo);
            autorun.Save(CheckBox_autorun.IsChecked.ToString());
            CheckBox_autorun.Checked += CheckBox_autorun_Checked;
        }

        // загружаем список в листбоксы
        private void lstCars_Loaded(object sender, RoutedEventArgs e)
        {          
         //   lstCars.ItemsSource = _ListSounds;            
        }

        // добавление
        private void Add_Click(object sender, RoutedEventArgs e)
        {
     //       Sound s = new Sound() {ReminderName = "-",ReminderHour = 0, ReminderMinute = 0, ReminderChecked = true, ReminderSound = 0 };          
      //      _ListSounds.Add(s);          
        }

        // удаление
        private void Del_Click(object sender, RoutedEventArgs e)
        {
       //    _ListSounds.RemoveAt(0);
        }

        //выделение всего текста при клике
        private void reminderhour_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            TextBox textbox = sender as TextBox;
            textbox.SelectionStart = 0;
            textbox.SelectionLength = textbox.Text.Length;
        }



    }


}
